# NMDC

::: {.toctree maxdepth="2" caption="NMDC Overview:"}
chapters/overview/0_overview.rst
:::

::: {.toctree maxdepth="2" caption="Standards:"}
chapters/standards/schema_index.rst
chapters/standards/Metadata_Documentation_Overview.rst
chapters/standards/schema-validation.rst
chapters/standards/identifiers.rst
chapters/standards/NMDC_MIxS_Soil_documentation.rst
chapters/standards/credits.rst
:::

::: {.toctree maxdepth="2" caption="NMDC Workflows:"}
chapters/workflows/overview.rst chapters/workflows/1_RQC_index.rst
chapters/workflows/2_ReadAnalysis_index.rst
chapters/workflows/3_MetaGAssemly_index.rst
chapters/workflows/4_MetaGAnnotation_index.rst
chapters/workflows/5_MAG_index.rst chapters/workflows/6_MetaT_index.rst
chapters/workflows/7_Metaproteomics_index.rst
chapters/workflows/8_Metabolomics_index.rst
chapters/workflows/9_NOM_index.rst
:::

::: {.toctree maxdepth="2" caption="NMDC Portal:"}
chapters/portal_guide/portal_guide.rst
:::
